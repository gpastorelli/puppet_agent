# puppet_agent::config
#
# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include puppet_agent::config
class puppet_agent::config (
  $service_ensure = $puppet_agent::service_ensure,
  $service_enable = $puppet_agent::service_enable,
  $server = $puppet_agent::server,
  $conf_path = $puppet_agent::conf_path,
) {

  service { 'puppet':
    ensure => $service_ensure,
    enable => $service_enable,
  }

  file { "${conf_path}/puppet.conf":
    ensure  => file,
    content => epp("${module_name}/puppet.conf_${facts['kernel']}.epp", { server => $server, }),
    notify  => Service['puppet'],
  }

}
