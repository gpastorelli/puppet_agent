# puppet_agent
#
# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include puppet_agent
class puppet_agent (
  Enum['running', 'stopped'] $service_ensure,
  Boolean $service_enable,
  String $server,
  String $conf_path,
) {

  include puppet_agent::config

}
